package application;
	
import com.sun.prism.shader.Texture_LinearGradient_REFLECT_AlphaTest_Loader;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
/*TreeView - � uma lista de Objetos especial baseado numa hierarquia de n�s
 *  -h� um n� raiz que tem n�s filhos e assim sucessivmente
 *  -branches - ramos, come�am num n� termina noutro
 *  -leaf - n� sem ramos descendentes
 *  -Sibiling - n�s irm�os filhos de um mesmo n� pai
 *  
 *  Processo :
 *  P1: Criar os n�s por : TreeItem<obj> xpto = new TreeItem<>("Nome
 *  P2: Definir quem � filho de quem;
 *  P3: Criar a arvore e associr o n� raiz � arvore :
 *  		TreeView <obj> arvore = new TreeView<> ("nome n� Raiz")
 *  P4: Listener para a tree.
 *  
 *  Nota : Vamos precisar da alertBox de Utils
 * 
 * 	*/

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			
			//Passo 1 Criar N� Raiz.
			TreeItem<String> noRaiz = new TreeItem<>("raiz");		//N� com nome raiz
			noRaiz.setExpanded(true);								//True: Define inicio expandido.
			
			//Criar N�s para adicionar � raiz
			TreeItem<String> no1 = new TreeItem<>("No1");			//N� com nome raiz no1
			TreeItem<String> no2 = new TreeItem<>("No2");			//No com nome raiz no2
			noRaiz.getChildren().addAll(no1,no2);					//Associa-los ao n� raiz
			
			no1.getChildren().addAll(
					new TreeItem<String>("subItem 1"),				//Objetos an�nimos, Sem nome
					new TreeItem<String>("subItem 2"),
					new TreeItem<String>("subItem 3")
			);
			//Passo 3 Criar a treee e adicionar raiz
			TreeView<String> tree = new TreeView<>(noRaiz);
			tree.setShowRoot(true);
			
			
			//Passo4: Listener para a tree - N�o TEM SETONACTION()
			//porque multiplos itens e permite v�rios tipos de sele��o: simples, multipla.
			tree.getSelectionModel().selectedItemProperty().addListener( (v, oldItem, newItem) ->{
				
				if(newItem != null){
					Utils.alertBox("Sele��o", "Op��o Selecionada:\n"+ newItem.getValue());
				}
			});
			
			StackPane layout = new StackPane();
			layout.setPadding(new Insets(20,20,20,20));
			layout.getChildren().add(tree);
			
			
			BorderPane root = new BorderPane();
			Scene scene = new Scene(layout,300,200);
			primaryStage.setTitle("Tree View");
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
